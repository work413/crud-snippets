# CRUD Snippets

This web application manages code [snippets](https://en.wikipedia.org/wiki/Snippet_(programming)). The web application will use the Node.js platform, Express as the application framework, and Mongoose as the object data modeling (ODM) library for MongoDB.

## The web application

The web application is a Node.js application that uses Express as the application framework and Mongoose as the ODM to create a web application that can store data persistently.

The web application's data is stored by a MongoDB database.

Users must be able to register and login into the application after entering a username and a password. The username must be unique to the application and the password must not be able to be read back. A logged-in user must be able to log out of the application.

For the application to differentiate between an anonymous and authenticated user, there are support for some basic authentication and authorization.

The web application have full CRUD functionality regarding snippets, whereby a user is able to create, read, update, and delete snippets. Anonymous users are only able to view snippets. In addition to view snippets, authenticated users must also create, edit, and delete their snippets. __No one but the owner, the creator, of a snippet, is able to edit and delete the said snippet.__ When creating and viewing snippets, __the application supports multiline text__, enabling the logged-on user to write real code snippets, not just one-line texts.

If a user requests a non-existent resource or a resource that requires the user to be authenticated, the application returns the HTTP status code 404 (Not Found). When an authenticated user does not have permission to access a requested resource, the application returns the HTTP status code 403 (Forbidden). The HTTP status code 500 (Internal Server Error) is only returned when it is really necessary.

The application should be deployed on a given server.
