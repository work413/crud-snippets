/**
 * Database connection.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

/**
 * Connects to mongoDB database.
 *
 * @returns {Promise} Does something if connection is made.
 */
export const connectDB = async () => {
  mongoose.connection.on('error', err =>
    console.log(`Mongoose error: ${err}`)
  )

  return mongoose.connect(process.env.DB_CONNECTION_STRING, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
}
