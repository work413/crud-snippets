/**
 * Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import { UserModel } from '../models/new-user.js'
import { SnippetModel } from '../models/snippet.js'

/**
 * User Controller class.
 */
export class UserController {
  /**
   * Renders the login page.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async login (req, res, next) {
    try {
      res.render('crud-pages/login')
    } catch (error) {
      next(error)
    }
  }

  /**
   * Logs in the user.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async postLogin (req, res, next) {
    try {
      const registeredUser = await UserModel.authenticate(req.body.name, req.body.passphrase)
      // Does something if a registered user is found.
      if (registeredUser) {
        req.session.regenerate(function (err) {
          if (err) {
            console.log(err)
          }
        })
        req.session.authenticated = true
        req.session.save()

        req.session.name = registeredUser.nickname // This is used when setting and finding the author for a code snippet.
      }
      res.redirect('.')
    } catch (error) {
      req.session.flash = { message: 'Login failed.' }
      res.redirect('/login')
    }
  }

  /**
   * Renders create account page.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function
   */
  async createAccount (req, res, next) {
    try {
      res.render('crud-pages/create-account')
    } catch (error) {
      next(error)
    }
  }

  /**
   * Creates new user object and saves it to the database.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   */
  async postCreateUser (req, res) {
    try {
      const user = new UserModel({})
      // Checks if the entered passphrases match.
      if (req.body.passphrase === req.body.passRepeat) {
        // Calls the function that checks if there already is a user by the chosen name.
        await UserModel.uniqueUser(req.body.nickname)
        user.nickname = req.body.nickname
        user.passphrase = req.body.passphrase
      } else {
        req.session.flash = { message: 'The passphrases did not match.' }
        res.redirect('create-account')
      }

      await user.save()
      req.session.flash = { message: 'Account successfully created.' }
      res.redirect('login')
    } catch (error) {
      req.session.flash = { message: 'Failed to create account. Try again.' }
      res.redirect('create-account')
    }
  }

  /**
   * Renders a page that displays all snippets written by the current user.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function
   */
  async ownSnippets (req, res, next) {
    try {
      const viewData = {
        // An array of objects containing data about the snippets.
        all: (await SnippetModel.find({}))
          .map(obj => ({
            id: obj._id,
            title: obj.title,
            content: obj.content,
            author: obj.author
          }))
      }
      const ownSnippets = {
        all: []
      }
      viewData.all.forEach(snippet => {
        if (snippet.author === req.session.name && req.session.name) {
          // Adds the current object to the array if it's author is the current user.
          ownSnippets.all.push(snippet)
        }
      })
      const registeredUser = true
      res.render('hidden/my-snippets', { ownSnippets, registeredUser })
    } catch (error) {
      next(error)
    }
  }

  /**
   * Destroys the current session and redirects back to the home page.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function
   */
  async logout (req, res, next) {
    try {
      await req.session.destroy()
      res.redirect('.')
    } catch (error) {
      next(error)
    }
  }
}
