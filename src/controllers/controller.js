/**
 * Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

/**
 * Controller class.
 */
export class Controller {
  /**
   * Authenticates the user and calls the next function after.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async authenticate (req, res, next) {
    if (req.session.authenticated) {
      next()
    } else {
      const error = new Error('Not Found')
      error.status = 404
      next(error)
    }
  }

  /**
   * Renders the home page.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async index (req, res, next) {
    try {
      // The display of the home page and menu depends on whether or not the user is authenticated.
      if (req.session.authenticated) {
        const registeredUser = true
        res.render('crud-pages/index', { registeredUser })
      } else {
        res.render('crud-pages/index')
      }
    } catch (error) {
      next(error)
    }
  }

  /**
   * Destroys the current session.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async destroy (req, res, next) {
    try {
      await req.session.destroy()
      next()
    } catch (error) {
      next(error)
    }
  }
}
