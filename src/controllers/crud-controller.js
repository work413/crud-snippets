/**
 * Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import { SnippetModel as Snippet } from '../models/snippet.js'

/**
 * Controller class.
 */
export class SnippetController {
  /**
   * Authorizes the user when trying to make changes.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async authorizeRights (req, res, next) {
    const snippetObj = await Snippet.findOne({ _id: req.params.id })
    // If the current is the author, call the next function.
    if (snippetObj.author === req.session.name) {
      next()
    } else {
      const error = new Error('Forbidden')
      error.status = 403
      next(error)
    }
  }

  /**
   * Shows all snippets.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async allSnippets (req, res, next) {
    try {
      const viewData = {
        all: (await Snippet.find({}))
          .map(obj => ({
            id: obj._id,
            title: obj.title,
            content: obj.content,
            author: obj.author
          }))
      }
      // Checks if the current user is the author, and sets ownedByCurrent to true if that is the case.
      viewData.all.forEach(snippet => {
        if (snippet.author === req.session.name && req.session.name) {
          snippet.ownedByCurrent = true
        }
      })
      // The display of the menu depends on whether or not the user is authenticated.
      if (req.session.authenticated) {
        const registeredUser = true
        res.render('crud-pages/all-snippets', { viewData, registeredUser })
      } else {
        res.render('crud-pages/all-snippets', { viewData })
      }
    } catch (error) {
      next(error)
    }
  }

  /**
   * Rendera a page for creating a new snippet.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async createSnippet (req, res, next) {
    try {
      const registeredUser = true
      res.render('hidden/create', { registeredUser })
    } catch (error) {
      next(error)
    }
  }

  /**
   * Creates and saves the snippet.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async postCreate (req, res, next) {
    try {
      const newSnippet = new Snippet({
        content: req.body.new,
        title: req.body.title,
        author: req.session.name
      })
      await newSnippet.save()
      req.session.flash = { message: 'The code snippet was successfully created' }
      res.redirect('.')
    } catch (error) {
      req.session.flash = { message: 'Failed to create.' }
      res.redirect('hidden/create')
    }
  }

  /**
   * Handles the edit.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async editSnippet (req, res, next) {
    try {
      // Finds the object to edit.
      const edit = await Snippet.findOne({ _id: req.params.id })
      const viewData = {
        title: edit.title,
        id: edit._id,
        content: edit.content,
        done: edit.done
      }
      const registeredUser = true
      res.render('./hidden/edit', { viewData, registeredUser })
    } catch (error) {
      next(error)
      res.render('.')
    }
  }

  /**
   * Handles the update.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async updateSnippet (req, res, next) {
    try {
      const update = await Snippet.updateOne({ _id: req.body.id }, {
        content: req.body.content
      })
      if (update) {
        req.session.flash = { message: 'The code snippet was successfully updated' }
      }
      res.redirect('..')
    } catch (error) {
      next(error)
    }
  }

  /**
   * Find object to delete.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async deleteSnippet (req, res, next) {
    try {
      // Finds the object to delete.
      const delObj = await Snippet.findOne({ _id: req.params.id })
      const viewData = {
        id: delObj._id,
        content: delObj.content,
        done: delObj.done
      }
      const registeredUser = true
      res.render('hidden/delete', { viewData, registeredUser })
    } catch (error) {
      next(error)
    }
  }

  /**
   * Send a delete request.
   *
   * @param {object} req - Request object.
   * @param {object} res - Response object.
   * @param {Function} next - Next function.
   */
  async postDelete (req, res, next) {
    try {
      await Snippet.deleteOne({ _id: req.params.id })
      req.session.flash = { message: 'The code snippet was successfully deleted.' }
      res.redirect('..')
    } catch (error) {
      req.session.flash = { message: 'Failed to delete.' }
      res.redirect('.')
    }
  }
}
