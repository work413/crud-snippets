/**
 * Mongoose schema
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'

// Schema for user object.
const user = new mongoose.Schema({
  type: Object,
  nickname: {
    type: String,
    required: true,
    unique: true,
    maxLength: [30, 'Nickname cannot have more than 30 characters.']
  },
  passphrase: {
    type: String,
    required: true,
    minLength: [6, 'Passphrase must be at least 6 charcters long'],
    maxLength: [100, 'Passphrase cannot have more than 100 characters.']
  }
})

/**
 * Authenticates the user.
 *
 * @param {string} name - User nickname.
 * @param {string} pass - User passphrase.
 * @returns {object} - The user object.
 */
user.statics.authenticate = async function (name, pass) {
  const user = await this.findOne({ nickname: name })

  if (user && await bcrypt.compare(pass, user.passphrase)) {
    return user
  } else {
    throw new Error()
  }
}

/**
 * Checks if the username is already in use.
 *
 * @param {string} name - User nickname.
 * @returns {boolean} - Returns true if username is unique.
 */
user.statics.uniqueUser = async function (name) {
  const user = await this.findOne({ nickname: name })
  if (user) {
    throw new Error()
  } else {
    return true
  }
}

// Creates a hashed version of the passphrase.
user.pre('save', async function () {
  this.passphrase = await bcrypt.hash(this.passphrase, 8)
})

// Create model from schema.
export const UserModel = mongoose.model('User', user)
