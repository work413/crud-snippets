/**
 * Mongoose schema
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

// Schema for snippet object.
const snippet = new mongoose.Schema({
  type: Object,
  title: {
    type: String,
    maxLength: [50, 'Title cannot be longer than 50 characters.']
  },
  content: {
    type: String,
    required: true,
    minLength: [1, 'Snippet must be at least 1 character long.']
  },
  author: {
    type: String,
    required: true
  },
  done: {
    type: Boolean,
    required: true,
    default: false
  }
})

// Create model from schema.
export const SnippetModel = mongoose.model('Snippet', snippet)
