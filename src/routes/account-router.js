/**
 * Router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { UserController } from '../controllers/account-controller.js'
import { Controller } from '../controllers/controller.js'

export const router = express.Router()
const uController = new UserController()
const controller = new Controller()

router.get('/login', controller.destroy, uController.login)
router.post('/login', uController.postLogin)

router.get('/create-account', controller.destroy, uController.createAccount)
router.post('/create-user', uController.postCreateUser)

router.get('/my-snippets', controller.authenticate, uController.ownSnippets)

router.get('/logout', uController.logout)
