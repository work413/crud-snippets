/**
 * Router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { Controller } from '../controllers/controller.js'
import { SnippetController } from '../controllers/crud-controller.js'

export const router = express.Router()
const controller = new Controller()
const sController = new SnippetController()

router.get('/', controller.index)

router.get('/create', controller.authenticate, sController.createSnippet)
router.post('/create', sController.postCreate)

router.get('/all-snippets', sController.allSnippets)

router.get('/:id/edit', controller.authenticate, sController.authorizeRights, sController.editSnippet)

router.post('/:id/update', sController.updateSnippet)

router.get('/:id/delete-this', controller.authenticate, sController.authorizeRights, sController.deleteSnippet)

router.post('/:id/delete', sController.postDelete)
