/**
 * Router.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import createError from 'http-errors'
import { router as crudRouter } from './crud-router.js'
import { router as userRouter } from './account-router.js'

export const router = express.Router()

router.use('/', crudRouter)
router.use('/', userRouter)

router.use('*', (req, res, next) => next(createError(404)))
