/**
 * The starting point of the application.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import hbs from 'express-hbs'
import helmet from 'helmet'
import session from 'express-session'
import logger from 'morgan'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { connectDB } from './config/mongoose.js'
import { router } from './routes/router.js'

/**
 * The main function of the application.
 */
const main = async () => {
  await connectDB()

  const app = express()

  const directoryFullName = dirname(fileURLToPath(import.meta.url))

  app.use(helmet())

  app.use(logger('dev'))

  // The view engine directories.
  app.engine('hbs', hbs.express4({
    defaultLayout: join(directoryFullName, 'views', 'default', 'default'),
    partialsDir: join(directoryFullName, 'views', 'partials')
  }))

  app.set('view engine', 'hbs')

  app.set('views', join(directoryFullName, 'views'))

  // Makes the body available in the request object.
  app.use(express.urlencoded({ extended: false }))

  // Use the static files.
  app.use(express.static(join(directoryFullName, '..', 'static')))

  // Session information object.
  const sInfo = {
    name: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24,
      sameSite: 'lax'
    }
  }

  app.use(session(sInfo))

  // Handles flash messages.
  app.use((req, res, next) => {
    if (req.session.flash) {
      res.locals.flash = req.session.flash
      delete req.session.flash
    }
    next()
  })

  app.use('/', router)

  // Handles errors.
  app.use(function (err, req, res, next) {
    if (err.status === 404) {
      return res.status(404).sendFile(join(directoryFullName, 'views', 'error', '404.html'))
    }

    if (err.status === 403) {
      return res.status(403).sendFile(join(directoryFullName, 'views', 'error', '403.html'))
    }

    if (req.app.get('env') !== 'development') {
      return res.status(500).sendFile(join(directoryFullName, 'views', 'error', '500.html'))
    }
  })

  // Starts the server.
  app.listen(process.env.PORT, () => {
    console.log(`Server running at http://localhost:${process.env.PORT}`)
    console.log('Press Ctrl-C to terminate...')
  })
}

main().catch(console.error)
