const passVisability = document.getElementById('toggle')
const cookie = document.querySelector('.cookie-consent')
const cookieBtn = document.getElementById('cookie-btn')

// Toggles the visability of the password.
if (passVisability) {
  passVisability.addEventListener('click', () => {
    const password = document.getElementById('password')
    password.type = password.type === 'password' ? 'text' : 'password'
  })
}

// Cookie consent pop-up.
if (cookie) {
  setTimeout(() => {
    const cookieClosed = localStorage.getItem('cookieClosed')
    if (!cookieClosed) {
      cookie.classList.add('visable')
    }
  }, 1500)
  cookieBtn.addEventListener('click', () => {
    cookie.classList.remove('visable')
    localStorage.setItem('cookieClosed', true)
  })
}
